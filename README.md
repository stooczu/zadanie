# Test application

1. Backend build in Laravel
2. Frontend build in Vue.js

## Installation & run

1. Copy repository by:

```
git clone https://gitlab.com/stooczu/zadanie.git
```

2. Copy `.env` file by:

```
cp .env.example .env
```

3. Fill **username** and **password** for DB in `.env` file (don't use `root` as username).
4. Run installation script by:

```
make install
```

5. Open link `http://0.0.0.0:80` in browser

### Starting application

1. Execute command:

```
make up
```

2. Open link `http://0.0.0.0:80` in browser

### Stopping application

```
make down
```

## API documentation

API documentation is available at: `http://0.0.0.0:80/docs`

## Testing

Execute command:
```
make test
```
to test application
