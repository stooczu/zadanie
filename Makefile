EXEC=docker-compose exec app

install:
	docker-compose up -d
	${EXEC} composer install
	${EXEC} npm install
	${EXEC} npm run build
	${EXEC} php artisan key:generate
	${EXEC} php artisan jwt:secret --force
	${EXEC} php artisan migrate:fresh --seed

up:
	docker-compose up -d
	@echo "Open http://0.0.0.0 in you browser"

down:
	docker-compose down

test:
	docker-compose exec app php artisan test
