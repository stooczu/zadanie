<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>{{env('APP_NAME')}}</title>

    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
    <div id="app">
        @yield('content')
    </div>
    <script src="/js/app.js"></script>
</body>
</html>
