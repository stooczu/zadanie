export const Endpoints = {
    Auth: {
        Login: 'login',
        Me: 'me',
    },
    Cities: 'cities',
}
