import axios from "axios";

const API_VERSION = 'api/v1/'

export default function (){
    axios.defaults.baseURL = API_VERSION
    axios.defaults.headers.common = {
        'Authorization': `Bearer ` + localStorage.getItem('jwt-token'),
        'Accept': 'application/json'
    }
    return axios
}
