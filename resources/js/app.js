import './bootstrap.js';
import {createApp} from 'vue';
import router from './plugins/router'
import AppVue from './components/App.vue';
import Toaster from '@meforma/vue-toaster';


const app = createApp({});

app.component('app-vue', AppVue)
    .use(router)
    .use(Toaster, {
        position: 'top'
    })
    .mount('#app');

