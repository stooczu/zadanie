import Login from "../components/Auth/Login.vue";
import {createRouter, createWebHashHistory} from "vue-router"
import Cities from "../components/Cities.vue";

function ensureIsAuthenticated() {
    return localStorage.getItem('jwt-token') !== null;
}

const routes = [
    { path: '/', component: Login , name: 'login'},
    { path: '/cities', component: Cities, name: 'cities', beforeEnter: [ensureIsAuthenticated] },
];

const HOME = { name: 'cities' };

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

router.beforeEach(async (to, from) => {
    const isAuthenticated = ensureIsAuthenticated();

    if (!isAuthenticated && to.name !== 'login') {
        return { name: 'login' };
    }
    if (isAuthenticated && to.name === 'login') {
        return HOME;
    }
})

export default router;

