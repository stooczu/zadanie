<?php

use App\Http\Controllers\CityController;
use App\Modules\Auth\Controllers\MeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('v1')->middleware('throttle:100,5')->group(function () {

    include('api/auth.php');

    Route::middleware('auth:api')->group(function () {
        Route::get('me', MeController::class);
        Route::get('cities', [CityController::class, 'index']);
    });
});
