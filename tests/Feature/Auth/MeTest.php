<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class MeTest extends TestCase
{
    public function test_authenticated_user_can_fetch_me_data()
    {

        $this->actingAs(User::factory()->create());

        $meResponse = $this->getJson('api/v1/me');

        $meResponse->assertOk();
        $meResponse->assertJsonStructure([
            'name',
            'email',
            'email_verified_at'
        ]);
    }

    public function test_if_unauthenticated_user_can_not_fetch_me_data(){
        $credentials = [
            'email' => $this->faker->email(),
            'password' => $this->faker->password(),
        ];
        $invalidCredentials = [
            'email' => $credentials['email'],
            'password' => $this->faker->password(),
        ];
        User::factory()->create([
            'email' => $credentials['email'],
            'password' => Hash::make($credentials['password'])
        ]);

        $loginResponse = $this->postJson('api/v1/login', $invalidCredentials);
        $token = $loginResponse->json('token');
        $meResponse = $this->getJson('api/v1/me', headers: ['Authorization' => `Bearer: $token`]);

        $meResponse->assertUnauthorized();;
    }
}
