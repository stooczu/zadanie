<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tests\Feature\Auth\TestCase as BaseTest;

class LoginTest extends BaseTest
{

    public function test_user_can_log_in_with_correct_credentials()
    {
        $credentials = [
            'email'    => $this->faker->email(),
            'password' => Hash::make($this->faker->password())
        ];
        User::factory()->create([
            'email'    => $credentials['email'],
            'password' => Hash::make($credentials['password'])
        ]);

        $loginResponse = $this->postJson('api/v1/login', $credentials)->assertOk();

        $this->assertAuthenticated('api');
        $this->assertNotNull($loginResponse->json('token'));
    }

    public function test_user_can_not_log_in_with_incorrect_credentials()
    {

        $user = User::factory()->create([
            'email'    => $this->faker->email(),
            'password' => Hash::make($this->faker->password())
        ]);

        $invalidCredentials = [
            'email'    => $user->email,
            'password' => $this->faker->password(),
        ];

        $this->postJson('api/v1/login', $invalidCredentials)
            ->assertBadRequest()
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function test_user_can_not_log_in_providing_any_credentials()
    {
        $this->postJson('api/v1/login')
            ->assertUnprocessable()
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'email'    => [],
                    'password' => []
                ]
            ]);
    }
}
