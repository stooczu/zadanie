<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class CityTest extends TestCase
{
    public function test_can_get_cities_list()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->getJson('/api/v1/cities');
        $response->assertStatus(200)->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'description',
                    'population'
                ],
            ]
        ]);
    }
}
