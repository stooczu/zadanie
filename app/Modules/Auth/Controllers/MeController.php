<?php

namespace App\Modules\Auth\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MeController extends Controller
{
    /**
     * User data
     *
     * @group Auth
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request) {
        return $request->user();
    }
}
