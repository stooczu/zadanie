<?php

declare(strict_types=1);

namespace App\Modules\Auth\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Get token
     *
     * User is authenticated via email and password pair
     *
     * @group Auth
     * @unauthenticated
     *
     * @bodyParam email string required Example: test@example.com
     * @bodyParam password string required Example: password
     *
     *
     * @response 200 {
     *      "token": "31|idwFTfzgxLfoB9vBP3lSXQ6dcP7Ns5AVHyuCs5KE"
     *      "type": "bearer"
     * }
     */
    public function __invoke(Request $request) {
        $validated = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $token = Auth::attempt([
            'email'    => $validated['email'],
            'password' => $validated['password']
        ]);

        if (!($token)) {
            return response()->json([
                'message' => __('Incorrect credentials')
                ], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([
            'token' => $token,
            'type' => 'bearer',
        ], Response::HTTP_OK);
    }
}
