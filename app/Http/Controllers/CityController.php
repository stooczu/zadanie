<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Http\Request;

/**
 * @group Cities
 *
 */
class CityController extends Controller
{

    /**
     * Index
     */
    public function index()
    {
        return CityResource::collection(City::query()->paginate());
    }

}
